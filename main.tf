terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-west-3"
}

resource "aws_key_pair" "deployer" {
  key_name   = "terraform-aws"
  public_key = file("~/.ssh/terraform-aws.pub")
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins-sg"
  description = "Security group for Jenkins"
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "jenkins-sg"
  }
}

resource "aws_vpc_security_group_ingress_rule" "jenkins-sg_ssh_ipv4" {
  security_group_id = aws_security_group.jenkins-sg.id
  cidr_ipv4         = "141.94.138.152/32"
  from_port         = 22
  to_port           = 22
  ip_protocol       = "tcp"
}

resource "aws_vpc_security_group_ingress_rule" "jenkins-sg_http" {
  security_group_id = aws_security_group.jenkins-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 80
  to_port           = 80
  ip_protocol       = "tcp"
}

resource "aws_vpc_security_group_ingress_rule" "jenkins-sg_https" {
  security_group_id = aws_security_group.jenkins-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 443
  to_port           = 443
  ip_protocol       = "tcp"
}

resource "aws_instance" "terraform-jenkins" {
  ami             = "ami-087da76081e7685da"
  instance_type   = "t2.micro"
  vpc_security_group_ids = [aws_security_group.jenkins-sg.id]
  tags = {
    Name = "terraform-jenkins"
  }
  key_name = "terraform-aws"
}

resource "local_file" "hosts" {
    content = templatefile("./templates/hosts.tpl",
      {
        ip = aws_instance.terraform-jenkins.public_ip
      }
    )
    filename = "./hosts"
}
